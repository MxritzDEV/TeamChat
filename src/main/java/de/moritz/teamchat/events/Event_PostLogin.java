package de.moritz.teamchat.events;

import de.moritz.teamchat.TeamChat;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author MxritzDEV
 */

public class Event_PostLogin implements Listener {

    @EventHandler
    public void onPostLogin( PostLoginEvent event ) {
        ProxiedPlayer player = event.getPlayer();

        if ( !player.hasPermission( "server.team" ) )
            return;

        for ( ProxiedPlayer all : ProxyServer.getInstance().getPlayers() ) {
            if ( !all.hasPermission( "server.team" ) )
                continue;
            if ( TeamChat.getNoNotify().contains( all.getUniqueId() ) )
                continue;
            all.sendMessage( TeamChat.getPrefix() + "§c" + player.getName() + " §7ist dem Server beigetreten" );
        }
    }
}
