package de.moritz.teamchat.commands;

import de.moritz.teamchat.TeamChat;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author MxritzDEV
 */

public class CMD_Notify extends Command {

    public CMD_Notify( String name, String permission, String... aliases ) {
        super( name, permission, aliases );
    }

    @Override
    public void execute( CommandSender commandSender, String[] strings ) {
        if ( !( commandSender instanceof ProxiedPlayer ) ) {
            commandSender.sendMessage( "Du bist kein Spieler!" );
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        if ( strings.length != 0 ) {
            player.sendMessage( TeamChat.getPrefix() + "§cNutze: /notify" );
            return;
        }

        if ( TeamChat.getNoNotify().contains( player.getUniqueId() ) ) {
            TeamChat.getNoNotify().remove( player.getUniqueId() );
            player.sendMessage( TeamChat.getPrefix() + "§7Du bekommst nun wieder Teamnachrichten." );
            return;
        }
        TeamChat.getNoNotify().add( player.getUniqueId() );
        player.sendMessage( TeamChat.getPrefix() + "§7Du bekommst nun keine Teamnachrichten mehr." );
    }
}
