package de.moritz.teamchat.commands;

import de.moritz.teamchat.TeamChat;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author MxritzDEV
 */

public class CMD_TeamChat extends Command {

    public CMD_TeamChat( String name, String permission, String... aliases ) {
        super( name, permission, aliases );
    }

    @Override
    public void execute( CommandSender commandSender, String[] strings ) {
        if ( !( commandSender instanceof ProxiedPlayer ) ) {
            commandSender.sendMessage( "Du bist kein Spieler!" );
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        if ( strings.length == 0 ) {
            player.sendMessage( TeamChat.getPrefix() + "§cNutze: /teamchat <Message>" );
            return;
        }

        if ( TeamChat.getNoNotify().contains( player.getUniqueId() ) ) {
            player.sendMessage( TeamChat.getPrefix() + "§cDu kannst keine Teamnachrichten senden, da du " +
                    "diese deaktiviert hast!" );
            return;
        }

        StringBuilder message = new StringBuilder();
        for ( String string : strings ) {
            message.append( string ).append( " " );
        }

        for ( ProxiedPlayer all : ProxyServer.getInstance().getPlayers() ) {
            if ( !all.hasPermission( "server.team" ) )
                continue;
            if ( TeamChat.getNoNotify().contains( all.getUniqueId() ) )
                continue;
            all.sendMessage( TeamChat.getPrefix() + "§c" + player.getName() + "§7: §f" + message.toString() );
        }
    }
}
