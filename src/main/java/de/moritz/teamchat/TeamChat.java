package de.moritz.teamchat;

import com.google.common.collect.Lists;
import de.moritz.teamchat.commands.CMD_Notify;
import de.moritz.teamchat.commands.CMD_TeamChat;
import de.moritz.teamchat.events.Event_PlayerDisconnect;
import de.moritz.teamchat.events.Event_PostLogin;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.List;
import java.util.UUID;

/**
 * @author MxritzDEV
 */

public class TeamChat extends Plugin {

    private static TeamChat instance;
    private static String prefix;
    private static List<UUID> noNotify;

    @Override
    public void onEnable( ) {
        TeamChat.instance = this;
        TeamChat.prefix = "§f[§bTeamChat§f] §r";
        TeamChat.noNotify = Lists.newArrayList();

        ProxyServer.getInstance().getPluginManager().registerListener( this, new Event_PlayerDisconnect() );
        ProxyServer.getInstance().getPluginManager().registerListener( this, new Event_PostLogin() );

        ProxyServer.getInstance().getPluginManager().registerCommand( this, new CMD_TeamChat( "teamchat", "server.team"
                , "tc" ) );
        ProxyServer.getInstance().getPluginManager().registerCommand( this, new CMD_Notify( "notify", "server.team" ) );
    }

    public static TeamChat getInstance( ) {
        return instance;
    }

    public static String getPrefix( ) {
        return prefix;
    }

    public static List<UUID> getNoNotify( ) {
        return noNotify;
    }
}
